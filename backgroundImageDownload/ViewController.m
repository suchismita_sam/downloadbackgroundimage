//
//  ViewController.m
//  backgroundImageDownload
//
//  Created by Click Labs134 on 12/2/15.
//  Copyright (c) 2015 clicklabs. All rights reserved.
//

#import "ViewController.h"

NSMutableArray *data;

@interface ViewController ()
@property (strong, nonatomic) IBOutlet UIImageView *backgroundImage;
@property (strong, nonatomic) IBOutlet UIButton *showBackgroundImageButton;
@property (strong, nonatomic) IBOutlet UISearchBar *givePicUrl;

@end

@implementation ViewController

@synthesize backgroundImage;
@synthesize showBackgroundImageButton;
@synthesize givePicUrl;

- (void)viewDidLoad {
    [super viewDidLoad];
    indicator.hidden=YES;

    // Do any additional setup after loading the view, typically from a nib.
}
- (IBAction)downloadImage:(id)sender
{

    NSLog(@"Hi");
    indicator.hidden=NO;
    [self performSelector:@selector(getApi) withObject:nil afterDelay:2.0];
}

-(void) getApi{
    
    NSURL *url=[NSURL URLWithString:@"http://www.spwallpapers.com/var/albums/640x1136/Kawaii%20cartoon%20style%20iPhone5%20wallpapers%20640x1136/Kawaii%20cartoon%20style%20iPhone5%20wallpapers%20640x1136%20(15).jpg?m=1368847983"];
    NSURLRequest *request=[NSURLRequest requestWithURL:url];
    
    indicator.hidden=YES;
    
    AFHTTPRequestOperation *operation=[[AFHTTPRequestOperation alloc]initWithRequest:request];
    operation.responseSerializer=[AFImageResponseSerializer serializer];
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation,id responseObject)
     {
         backgroundImage.image=responseObject;
     }failure:^(AFHTTPRequestOperation *operation,NSError *error)
     {
         NSLog(@"Error:%@",error);
     }];
    [operation start];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
